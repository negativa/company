@extends('layouts.app')
@section('title')
Position
@stop

@section('content')

    <h1>Positions <a href="{{ url('positions/create') }}" class="btn btn-primary pull-right btn-sm">Add New Position</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblpositions">
            <thead>
                <tr>
                    <th>ID</th><th>Title</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($positions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('positions', $item->id) }}">{{ $item->title }}</a></td>
                    <td>
                        <a href="{{ url('positions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['positions', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblpositions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection