@extends('layouts.app')
@section('title')
Position
@stop

@section('content')

    <h1>Position</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Title</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $position->id }}</td> <td> {{ $position->title }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection