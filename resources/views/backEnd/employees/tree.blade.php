@extends('layouts.app')

@section('title')
Employee Tree
@stop

@section('content')
    @include('backEnd.employees.partials.tree', ['employees' => $employees])
@endsection