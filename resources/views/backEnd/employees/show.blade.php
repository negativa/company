@extends('layouts.app')
@section('title')
Employee
@stop

@section('content')

    <h1>Employee</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th>
                    <th>Full Name</th>
                    <th>Salary</th>
                    <th>Boss Id</th>
                    <th>Start Day</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ $employee->id }} </td>
                    <td> {{ $employee->full_name }} </td>
                    <td> {{ $employee->salary }} </td>
                    <td> {{ $employee->boss_id }} </td>
                    <td> {{ $employee->start_day }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection