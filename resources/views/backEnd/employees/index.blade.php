@extends('layouts.app')
@section('title')
Employee
@stop

@section('content')
    <h1>Employees <a href="{{ url('employees/create') }}" class="btn btn-primary pull-right btn-sm">Add New Employee</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered" id="employees-table">
            <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Position</th>
                    <th>Salary</th>
                    <th>Start Day</th>
                    <th>Boss</th>
                </tr>
            </thead>
        </table>
    </div>
@stop


@section('scripts')

<script>
$(function() {
    $('#employees-table').DataTable({
        bFilter: true,
        processing: true,
        serverSide: true,
        ajax: '{!! route('employees.data') !!}',
        columns: [
            { data: 'full_name', name: 'employees.full_name',
                render: function(data, type, row, meta){
                    if(type === 'display'){
                        data = '<a href="/employees/' + row.id + '/edit">' + data + '</a>';
                    }

                    return data;
                }
            },
            { data: 'position.title', name: 'position.title' },
            { data: 'salary', name: 'employees.salary' },
            { data: 'start_day', name: 'employees.start_day' },
            { 
                data: 'boss.full_name',
                name: 'boss_id',
                orderable: true,
                searchable: false,
                render: function(data, type, row, meta){
                    if(row.boss_id === 0){
                        data = 'no boss';
                    }

                    return data;
                }
            },
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        }
    });
});
</script>

@stop