<ul>
@foreach($employees as $employee)
        <li><a href="/employees/{{ $employee->id }}">{{ $employee->full_name }}</a>
            @if(count($employee->children) > 0 )
                @include('backEnd.employees.partials.tree', ['employees' => $employee->children])
            @endif
        </li>                   
@endforeach
</ul>