@extends('layouts.app')
@section('title')
Edit Employee
@stop

@section('content')

    <h1>Edit Employee</h1>
    <hr/>

    {!! Form::model($employee, [
        'method' => 'PATCH',
        'url' => ['employees', $employee->id],
        'class' => 'form-horizontal'
    ]) !!}

            <div class="form-group {{ $errors->has('full_name') ? 'has-error' : ''}}">
                {!! Form::label('full_name', 'Full Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('full_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('full_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('salary') ? 'has-error' : ''}}">
                {!! Form::label('salary', 'Salary: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('salary', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('salary', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('start_day') ? 'has-error' : ''}}">
                {!! Form::label('start_day', 'Start Day: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('start_day', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('start_day', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('position_id') ? 'has-error' : ''}}">
                {!! Form::label('position_id', 'Position: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('position_id', [$employee->position->id => $employee->position->title], null, ['class' => 'form-control', 'id' => 'position', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('boss_id') ? 'has-error' : ''}}">
                {!! Form::label('boss_id', 'Boss: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('boss_id', [$employee->boss->id => $employee->boss->full_name], null, ['class' => 'form-control', 'id' => 'boss', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-3">
                    {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection
@section('scripts')
<script type="text/javascript">

      $('#boss').select2({
        placeholder: 'Select a boss',
        ajax: {
          url: '{!! route('employees.select') !!}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.full_name,
                        id: item.id
                    }
                })
            };
          },
          cache: true,
          maximumSelectionLength: 1,
          allowClear: false,
          multiple: false,
        }
      });

      $('#position').select2({
        placeholder: 'Select a position',
        ajax: {
          url: '{!! route('positions.select') !!}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.title,
                        id: item.id
                    }
                })
            };
          },
          cache: true,
          maximumSelectionLength: 1,
          allowClear: false,
          multiple: false,
        }
      });

</script>
@endsection