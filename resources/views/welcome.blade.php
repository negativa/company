@extends('layouts.app')
@section('title')
Company
@stop
@section('styles')

    <style>
        html, body {
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
            margin-top: -70px;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>

@stop

@section('content')

<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            {{ config('app.name', 'Company') }} 
        </div>

        <div class="links">
            <a href="{{ route('employees.index') }}">Employees list</a>
            <a href="{{ route('employees.index') }}">Employees Tree View</a>
        </div>
    </div>
</div>

@stop