<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    const MAX_POSITIONS = 25;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Position::class, self::MAX_POSITIONS)->create();
    }
}
