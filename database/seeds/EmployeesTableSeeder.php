<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{

    const MAX_POSITIONS = 50;
    const MAX_EMPLOYEES = 5000;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = factory(App\Position::class, self::MAX_POSITIONS)->make()->each(function($position) {
            $position->save();
        });

        factory(App\Employee::class, self::MAX_EMPLOYEES)->make()
            ->each(function($employee, $key) use ($positions) {
                
                $find_id = rand(1, self::MAX_POSITIONS);
                
                $position = $positions->first(function($position) use ($find_id) {
                    return $position->id == $find_id;
                });

                if ($key < 5)
                    $employee->boss_id = 0;
                else
                    $employee->boss_id = rand(1, $key);
                
                $employee->position()->associate($position);
                $employee->save();
            });
    }
}
