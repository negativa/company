<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('employees', function(Blueprint $table) {
                $table->increments('id');
                $table->string('full_name');
                $table->decimal('salary', 6, 0);
                $table->integer('boss_id')->nullable();
                $table->timestamp('start_day');

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }

}
