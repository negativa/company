<?php

namespace App\Classes\Helpers;

use Illuminate\Support\Collection;

class TreeGenerator
{

    public static function build(Collection $data)
    {
        $dataByBossId = $data->groupBy('boss_id');

        return self::createTree($dataByBossId, $dataByBossId->first());

    }

    protected static function createTree(&$list, $parent){
        
        return $parent->map(function($item) use ($list) {
            if ( $list->get($item->id) ) {
                $item->children = self::createTree($list, $list[$item->id]);
                unset($list[$item->id]);
            }
            return $item;
        });
    }

}