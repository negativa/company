<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function employees()
    {
        return $this->hasMany('App\Employee');
    }

}
