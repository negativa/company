<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Employee;
use App\Classes\Helpers\TreeGenerator;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Log;

class EmployeesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('backEnd.employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['full_name' => 'required', 'salary' => 'required', 'boss_id' => 'required', 'position_id' => 'required']);

        Log::debug($request->all());

        Employee::create($request->all());

        Session::flash('message', 'Employee added!');
        Session::flash('status', 'success');

        return redirect('employees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $employee = Employee::findOrFail($id);

        return view('backEnd.employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->load('boss');
        $employee->load('position');

        return view('backEnd.employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['full_name' => 'required', 'salary' => 'required', 'boss_id' => 'required', 'position_id' => 'required']);

        Log::debug($request->all());

        $employee = Employee::findOrFail($id);
        $employee->update($request->all());

        Session::flash('message', 'Employee updated!');
        Session::flash('status', 'success');

        return redirect('employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);

        $employee->delete();

        Session::flash('message', 'Employee deleted!');
        Session::flash('status', 'success');

        return redirect('employees');
    }

    /**
     * Display a Tree ctructure of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTree()
    {
        $employees = Employee::with('position')->get();
        
        $tree = TreeGenerator::build($employees);

        return view('backEnd.employees.tree', ['employees' => $tree]);
        
    }

    public function ajaxData()
    {
        $model = Employee::with('position', 'boss');
        return Datatables::of($model)
            ->make(true);

    }

    public function ajaxSelect(Request $request)
    {
        $employees = [];

        if($request->has('q')){
            $search = $request->q;
            $employees = Employee::select("id","full_name")
                    ->where('full_name','LIKE',"%$search%")
                    ->get();
        }

        return response()->json($employees);
    }

}
