<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Position;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class PositionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $positions = Position::all();

        return view('backEnd.positions.index', compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.positions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', ]);

        Position::create($request->all());

        Session::flash('message', 'Position added!');
        Session::flash('status', 'success');

        return redirect('positions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $position = Position::findOrFail($id);

        return view('backEnd.positions.show', compact('position'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $position = Position::findOrFail($id);

        return view('backEnd.positions.edit', compact('position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['title' => 'required', ]);

        $position = Position::findOrFail($id);
        $position->update($request->all());

        Session::flash('message', 'Position updated!');
        Session::flash('status', 'success');

        return redirect('positions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $position = Position::findOrFail($id);

        $position->delete();

        Session::flash('message', 'Position deleted!');
        Session::flash('status', 'success');

        return redirect('positions');
    }

    public function ajaxData()
    {
        return Datatables::of(Position::query())
            ->make(true);
    }

    public function ajaxSelect(Request $request)
    {
        $employees = [];

        if($request->has('q')){
            $search = $request->q;
            $employees = Position::select("id","title")
                    ->where('title','LIKE',"%$search%")
                    ->get();
        }

        return response()->json($employees);
    }

}
