<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boss extends Model
{

	protected $table = 'employees';

    public function employees()
    {
        return $this->hasMany('App\Employee');
    }
}