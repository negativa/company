<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['full_name', 'salary', 'boss_id', 'start_day', 'position_id'];
    
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function boss()
    {
        return $this->belongsTo('App\Boss');
    }

    public function position()
    {
        return $this->belongsTo('App\Position');
    }

    public function getStartDayAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

}
