<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['web', 'auth']], function () {
	Route::resource('employees', 'EmployeesController');
	Route::resource('positions', 'PositionsController');
	Route::get('/datatables/employees', 'EmployeesController@ajaxData')->name('employees.data');
	Route::get('/select/employees', 'EmployeesController@ajaxSelect')->name('employees.select');
	Route::get('/select/positions', 'PositionsController@ajaxSelect')->name('positions.select');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tree/employees', 'EmployeesController@indexTree')->name('employees.tree');
